provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region

}

resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "main_vpc"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main_gw"
  }
}

resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet1_cidr
  availability_zone = var.az1

  tags = {
    Name = "Main"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet2_cidr
  availability_zone = var.az2

  tags = {
    Name = "Main"
  }
}

resource "aws_subnet" "subnet3" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet3_cidr
  availability_zone = var.az3

  tags = {
    Name = "Main"
  }
}

resource "aws_route_table" "table" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name       = "public_route_table"
    managed-by = "terraform"
  }
}

resource "aws_main_route_table_association" "main" {
  vpc_id         = aws_vpc.main.id
  route_table_id = aws_route_table.table.id
}

resource "aws_route" "default" {
  route_table_id         = aws_route_table.table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

resource "aws_route_table_association" "sub_table1" {

  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.table.id
}

resource "aws_route_table_association" "sub_table2" {

  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.table.id
}

resource "aws_route_table_association" "sub_table3" {

  subnet_id      = aws_subnet.subnet3.id
  route_table_id = aws_route_table.table.id
}

resource "aws_security_group" "web_app" {
  vpc_id = aws_vpc.main.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rds" {
  vpc_id = aws_vpc.main.id
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#resource "aws_db_security_group" "default" {
#  name = "rds_sg"
#
#  ingress {
#    security_group_id = aws_security_group.rds.id
#  }
#}

resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = [aws_subnet.subnet2.id, aws_subnet.subnet3.id]

  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_db_instance" "default" {
  allocated_storage      = var.db_storage
  storage_type           = "gp2"
  engine                 = "postgres"
  engine_version         = "9.6.11"
  instance_class         = var.db_instance_class
  db_subnet_group_name   = aws_db_subnet_group.default.id
  name                   = var.db_name
  username               = var.db_username
  password               = var.db_password
  vpc_security_group_ids = [aws_security_group.rds.id]
  multi_az               = false
  publicly_accessible    = false
  skip_final_snapshot    = true
  tags = {
    Name = "db instance"
  }
}


resource "aws_launch_configuration" "web_conf" {
  image_id                    = var.ami_id
  instance_type               = var.ec2_instance_type
  key_name                    = var.ec2_key_name
  security_groups             = [aws_security_group.web_app.id]
  associate_public_ip_address = true

  #user_data = filebase64("${path.module}/example.sh")
  #user_data = filebase64("env.sh")
  user_data = base64encode("#!/bin/bash -xe\nsudo /bin/chmod 666 /etc/environment\nsudo /bin/echo 'RDS_USERNAME=${var.db_username}' >> /etc/environment\nsudo /bin/echo 'RDS_PASSWORD=${var.db_password}' >> /etc/environment\nsudo /bin/echo 'RDS_DBNAME=${var.db_name}' >> /etc/environment\nsudo /bin/echo 'RDSHOST_NAME=${aws_db_instance.default.address}' >> /etc/environment\nsudo /bin/chmod 644 /etc/environment\n/usr/bin/touch /home/ubuntu/.rsyncd.secret\n/bin/echo '${var.rsync_passwd}' >> /home/ubuntu/.rsyncd.secret\n/bin/chmod 600 /home/ubuntu/.rsyncd.secret\n/usr/bin/rsync -rv ${var.rsync_user}@${var.rsync_hostname}::${var.rsync_repo_name} ${var.rsync_local_path} --password-file=/home/ubuntu/.rsyncd.secret --port=873\n/usr/bin/pip3 install -r /home/ubuntu/master/requirements.txt\nsudo -u ubuntu /usr/bin/screen -dmS 'web_app'\nsudo -u ubuntu /usr/bin/screen -x -S 'web_app' -p 0 -X stuff 'sudo /usr/bin/python3 /home/ubuntu/master/views.py\\n'")
}

resource "aws_elb" "elb" {
  name               = "terraform-load-balance"
  security_groups    = [aws_security_group.elb.id]
  #availability_zones = [var.az1]
  subnets            = [aws_subnet.subnet1.id]
  health_check {
    healthy_threshold   = var.elb_healthy_threshold
    unhealthy_threshold = var.elb_unhealthy_threshold
    timeout             = var.elb_timeout
    interval            = var.elb_interval
    target              = "HTTP:8080/"
  }
  listener {
    lb_port           = var.elb_external_port
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
}

resource "aws_security_group" "elb" {
  name = "terraform-example-elb"
  vpc_id = aws_vpc.main.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_autoscaling_group" "asg" {
  launch_configuration = aws_launch_configuration.web_conf.id
  #availability_zones   = [var.az1]
  vpc_zone_identifier = [aws_subnet.subnet1.id]
  min_size             = var.asg_min_size
  max_size             = var.asg_max_size
  load_balancers       = [aws_elb.elb.name]
  health_check_type    = "ELB"
  tag {
    key                 = "Name"
    value               = "webapp_autoscaling_group"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "policy_up" {
  name                   = "cpu-policy-up"
  autoscaling_group_name = aws_autoscaling_group.asg.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = var.autoscaling_cooldown
  policy_type            = "SimpleScaling"
}
resource "aws_cloudwatch_metric_alarm" "alarm_up" {
  alarm_name          = "cpu-alarm-up"
  alarm_description   = "cpu-alarm-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = var.autoscaling_eval_period
  statistic           = "Average"
  threshold           = var.autoscaling_up_threshold
  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.asg.name
  }
  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.policy_up.arn]
}
# scale down alarm
resource "aws_autoscaling_policy" "policy_down" {
  name                   = "example-cpu-policy-scaledown"
  autoscaling_group_name = aws_autoscaling_group.asg.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = var.autoscaling_cooldown
  policy_type            = "SimpleScaling"
}
resource "aws_cloudwatch_metric_alarm" "alarm_down" {
  alarm_name          = "example-cpu-alarm-scaledown"
  alarm_description   = "example-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = var.autoscaling_eval_period
  statistic           = "Average"
  threshold           = var.autoscaling_down_threshold
  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.asg.name
  }
  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.policy_down.arn]
}
