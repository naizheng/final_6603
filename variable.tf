variable "created_by" {
    default = "terraform"
}

variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "vpc_cidr" {
    default = "10.0.0.0/16"
}

variable "subnet1_cidr" {
    default = "10.0.1.0/24"
}
variable "subnet2_cidr" {
    default = "10.0.2.0/24"
}
variable "subnet3_cidr" {
    default = "10.0.3.0/24"
}

variable "az1" {
    default = "us-east-1a"
}
variable "az2" {
    default = "us-east-1b"
}
variable "az3" {
    default = "us-east-1c"
}

variable "db_storage" {
    default = 8
}

variable "db_instance_class" {
    default = "db.t2.micro"
}

variable "db_name" {
    default = "mydb"
}
variable "db_username" {}
variable "db_password" {}

variable "aws_region" {
    default = "us-east-1"
}

variable "ami_id" {}

variable "ec2_instance_type" {
    default = "t2.micro"
}

variable "ec2_key_name" {}

variable "ec2_volume_size" {
    default = 8
}

variable "asg_min_size" {
    default = 1
}

variable "asg_max_size" {
    default = 2
}

variable "rsync_user" {
    default = "iadt"
}

variable "rsync_passwd" {}

variable "rsync_hostname" {}

variable "rsync_repo_name" {
    default = "IADT"
}

variable "rsync_local_path" {
    default = "/home/ubuntu/"
}

variable "elb_healthy_threshold" {
    default = 2
}

variable "elb_unhealthy_threshold"{
    default = 2
}

variable "elb_timeout"{
    default = 3
}

variable "elb_interval"{
    default = 30
}

variable "elb_external_port"{
    default = 80
}

variable "autoscaling_cooldown"{
    default = 300
}

variable "autoscaling_eval_period"{
    default = 120
}

variable "autoscaling_up_threshold"{
    default = 70
}

variable "autoscaling_down_threshold"{
    default = 40
}